var mongoose = require('mongoose');
var ExcerciseSchema = new mongoose.Schema({

    //_id: { type: "String", required:true, unique:true},
    ejercicio: String,
    descripcion: String,
    url_video: String,
    url_foto: String,
    musculos: String,
    url_fotoMusculo: String,
    pertenece_IRM: Boolean

});
mongoose.model('Excercise', ExcerciseSchema);

module.exports = mongoose.model('Excercise');