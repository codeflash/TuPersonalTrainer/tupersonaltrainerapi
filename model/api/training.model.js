var mongoose = require('mongoose');
var TrainingSchema = new mongoose.Schema({

    //_id: { type: "String", required:true, unique:true},
    training_plan:
        [
            {
                mes:[
                    {
                        nro_mes:        Number,
                        descripcion:    String,

                        semana:[
                            {
                                nro_semana: Number,
                                dia:
                                    [
                                        {
                                            nro_dia: String,
                                            completado: Boolean,
                                            ejercicio:[
                                                {
                                                    id_ejercicio: String,
                                                    series: Number,
                                                    repeticiones: Number,
                                                    descanso: Number
                                                }
                                            ]
                                        }
                                    ]

                            }
                        ]

                    }
                ]

            }
        ]

});
mongoose.model('Training', TrainingSchema);

module.exports = mongoose.model('Training');