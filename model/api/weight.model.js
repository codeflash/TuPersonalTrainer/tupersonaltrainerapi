var mongoose = require('mongoose');
var WeightSchema = new mongoose.Schema({

    //_id: { type: "String", required:true, unique:true},
    weight_register:
        [
            {
                fecha: Date,
                peso: Number
            }
        ]

});
mongoose.model('Weight', WeightSchema);

module.exports = mongoose.model('Weight');