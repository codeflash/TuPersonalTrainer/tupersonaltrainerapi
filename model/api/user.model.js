var mongoose = require('mongoose');
var UserSchema = new mongoose.Schema({
//user information
    nombre:             String,
    correo:             {type:String, lowercase: true},
    password:           String,
    telefono:           String,
    objetivo:           { type: String, enum: ['masa','peso','forma'] },
    edad:               Number,
    altura:             Number,
    genero:             {type: String, enum:  ['m', 'f']},
    //constitucion:       { type: String, enum: ['delgado','normal','grueso'] },
    porcentaje_grasa:   Number,
    peso:               Number,
    //peso_objetivo:      Number,
// foraing documents
    id_nutritional:     {type:String},
    id_tips:            {type:String},
    id_training:        {type:String},
    id_weight:          {type:String},
//resources
    fotos: {
        LateralDerecho: String,
        LateralIzquierdo:String,
        Frontal:        String,
        Espaldas:       String
    },
//facebook access
    facebook: {
        id:             String,
        token:          String,
        email:          {type:String, lowercase: true},
        photo:          String
    },
//acount information
    estado:             {type:Boolean, default:true},
    tipo_cuenta:        { type: String, enum: ['prueba','pago'], default:'prueba' },
    fecha_creacion:     {type: Date, default:Date.now()},
    fecha_modificacion: Date,
    fecha_fin:          Date


});
mongoose.model('User', UserSchema);

module.exports = mongoose.model('User');