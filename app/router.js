
var auth = require('../controller/api/auth.controller');
var nutritional = require('../controller/api/nutritional.controller');
var tips = require('../controller/api/tips.controller');
var training = require('../controller/api/training.controller');
var weight = require('../controller/api/weight.controller');
var excercise = require('../controller/admin/excercise.controller');

// ROUTES FOR OUR API
// =============================================================================

// create our router
var express    = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
var verifyToken = require('../app/VerifyToken');

router.post('/register', auth.register);
router.get('/me',verifyToken, auth.me);
router.put('/me',verifyToken, auth.update);
router.post('/login', auth.login);

router.get('/me/nutritionalplan',verifyToken, nutritional.show);
router.get('/me/nutritionalplan/last',verifyToken, nutritional.showLast);

router.get('/me/tips',verifyToken, tips.show);

router.get('/me/trainingplan',verifyToken, training.show);
router.get('/me/trainingplan/last',verifyToken, training.showLast);
router.get('/me/trainingplan/last/excercise/:excercise_id', excercise.showOne);

router.get('/me/weight',verifyToken, weight.show);
router.get('/me/weight/last',verifyToken, weight.showLast);
router.post('/me/weight',verifyToken, weight.add);



/*
// User app
router.get('/user', userC.findAll);
router.get('/user/:user_id',userC.findOne);
router.post('/user', userC.create);
router.put('/user/:user_id', userC.updateUser);
router.delete('/user/:user_id', userC.deleteUser);


// Dishes get app
router.get('/dish', dishC.findAll);
router.get('/loc', dishC.findAllByNear);     //pruebas de location con el dish
router.get('/dish/:dish_id', dishC.findOne);
//router.get('/dish?lng=:lngDish&lat=:latDish', dishC.findAllByNear);
//router.get('/dish?name=:name', dishC.findAllByName);
//router.get('/dish?category=:DishCategory', dishC.findAllByType);
//router.get('/user/:id/dish/:id', dishC.findAllFromUser);
router.post('/dish', dishC.create);
router.put('/dish/:dish_id', dishC.update); // CAMBIA TODO
router.patch('/dish/:dish_id', dishC.updateSome);
router.delete('/dish/:dish_id', dishC.delete);
//Drishes - buyDishes app
router.get('/dish/:dish_id/buy', buy_dishC.findAllByDish), //muestra las compras por plato
    router.get('/dish/:dish_id/reviews',   buy_dishC.findDishReviews), //muestra los reviews

    //override para formatear las cabeceras
    //javascript croosside origin


// buy_dish app

router.get('/buy_dish', buy_dishC.findAll);
router.get('/buy_dish/:buy_dish_id',buy_dishC.findOne);
router.post('/buy_dish/:dish_id', buy_dishC.create);
router.put('/buy_dish/:buy_dish_id', buy_dishC.update);
router.delete('/buy_dish/:buy_dish_id', buy_dishC.delete);
*/

module.exports = router;