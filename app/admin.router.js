
var admin = require('../controller/admin/admin.controller');
var tips = require('../controller/admin/tips.controller');
var nutritional = require('../controller/admin/nutritional.controller');
var training = require('../controller/admin/training.controller');
var weight = require('../controller/admin/weight.controller');
var excercise = require('../controller/admin/excercise.controller');

// ROUTES FOR OUR API
// =============================================================================

// create our router
var express    = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
var verifyToken = require('../app/VerifyToken');

router.get('/login', function (req,res) {
    res.render('login')
} );
router.get('/users', function (req,res) {
    res.render('users')
} );
router.get('/client', admin.show);
router.get('/client/:user_id', admin.showProfile);
router.post('/client/:user_id/tips', tips.createOne);
router.post('/client/:user_id/nutrition', nutritional.createOne);

router.get('/excercise', excercise.show);
router.get('/excercise/new', excercise.new);
router.post('/excercise/new', excercise.save);
router.get('/excercise/:excercise_id', excercise.showOne);
router.post('/excercise/:excercise_id/edit', excercise.edit);
router.get('/excercise/:excercise_id/delete', excercise.delete);

module.exports = router;