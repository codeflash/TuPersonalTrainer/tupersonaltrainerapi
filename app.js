var express = require('express');
var app = express();
var path = require('path');
var db = require('./app/db');
var morgan     = require('morgan');
var router = require('./app/router');
var routeradmin = require('./app/admin.router');
var bodyParser = require('body-parser');
app.use('/api', router);
app.use('/admin', routeradmin);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
// configure app
app.use(morgan('dev')); // log requests to the console
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

module.exports = app;