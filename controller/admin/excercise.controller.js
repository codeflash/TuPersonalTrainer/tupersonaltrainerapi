// ExcerciseController.js

var Excercise = require('../../model/admin/excercise.model');


module.exports = {

    show: function (req, res) {

        Excercise.find({},function (err, excercises) {
            if (err) return res.status(500).send("There was a problem finding the user.");
            if (!excercises) return res.status(404).send("No user found.");

            console.log("ejercicios",excercises);
            res.render('excercise_show', {excercises:excercises})
        });




    },
    showOne: function (req, res) {

        Excercise.findById(req.params.excercise_id,function (err, excercise) {
            if (err) return res.status(500).send("There was a problem finding the user.");
            if (!excercise) return res.status(404).send("No user found.");

            console.log("ejercicios",excercise);
            res.render('excercise_edit', {excercise:excercise})
        });




    },
    new: function (req,res){

        res.render('excercise_new')
    },

    save: function (req,res){
        Excercise.create({
            ejercicio: req.body.ejercicio,
            descripcion: req.body.descripcion,
            url_video: req.body.url_video,
            url_foto: req.body.url_foto,
            musculos: req.body.musculos,
            url_fotoMusculo: req.body.url_fotoMusculo,
            pertenece_IRM: req.body.pertenece_IRM
        },
            function (err, excercise) {
                if (err) return res.status(500).send("There was a problem registering the user."+ err.message)

                //res.status(200).send(excercise);
            });

        res.redirect('/ADMIN/excercise')
    },

    edit: function (req,res){
        Excercise.findByIdAndUpdate(req.params.excercise_id, req.body ,function (err, excercise) {
                if (err) return res.status(500).send("There was a problem finding the excercise.");
                if (!excercise) return res.status(404).send("No excercise found.");

                res.redirect('/ADMIN/excercise');
            })
        },



    delete: function (req, res) {

        Excercise.remove({_id:req.params.excercise_id},function (err, excercise) {
            if (err) return res.status(500).send("There was a problem finding the user.");
            if (!excercise) return res.status(404).send("No user found.");

            console.log("ejercicios",excercise);
            res.redirect('/ADMIN/excercise')
        });




    },


};

