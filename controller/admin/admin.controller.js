
var User = require('../../model/api/user.model');
var Nutrition = require('../../model/api/nutritional.model');
var Tips = require('../../model/api/tips.model');
var Training = require('../../model/api/training.model');
var Weight = require('../../model/api/weight.model');
var Excercise = require('../../model/admin/excercise.model');
module.exports = {

    show: function (req, res) {
        User.find({}, function (err, users) {
                if (err) return res.status(500).send("There was a problem finding the user.");
                if (!users) return res.status(404).send("No user found.");


                //console.log(users);
                res.render('client', {users: users});
            }
        )
    },

    showProfile: function (req, res) {
        var user,tips;
        //console.log(req.params.user_id);
        User.findById(req.params.user_id, function (err, user) {
                if (err) return res.status(500).send("There was a problem finding the user.");
                if (!user) return res.status(404).send("No user found.");

                Tips.findById(user.id_tips, function (err, tips) {
                    if (err) return res.status(500).send("There was a problem finding the tips.");
                    if (!tips) return res.status(404).send("No tips found.");


                    Weight.findById(user.id_weight, function (err, weight) {
                        if (err) return res.status(500).send("There was a problem finding the weight.");
                        if (!weight) return res.status(404).send("No weight found.");


                        Excercise.find(function (err, excercises) {
                            if (err) return res.status(500).send("There was a problem finding the excercises.");
                            if (!excercises) return res.status(404).send("No excercises found.");

                            console.log(excercises);
                            //console.log(weight);
                            res.render('client_profile', {user:user, tips: tips.tip_messages, weights:weight.weight_register, excercises:excercises});
                        })
                        //console.log(weight);
                    })



                });
        /*    Nutrition.findById(user.id_nutritional, function (err, user) {
                if (err) return res.status(500).send("There was a problem finding the user.");
                if (!user) return res.status(404).send("No user found.");
            })

            Training.findById(req.params.user_id, function (err, user) {
                if (err) return res.status(500).send("There was a problem finding the user.");
                if (!user) return res.status(404).send("No user found.");
            })
            */
                console.log(tips);
                //res.render('client_profile', {user:user});
            }
        )
    }
};
