// AuthController.js
var express = require('express');
var router = express.Router();
var Weight = require('../../model/api/weight.model');
var User = require('../../model/api/user.model');





module.exports = {

    create: function (user_id) {

        User.findById(user_id, function (err, user) {
            if (err) return res.status(500).send("There was a problem finding the user.");
            if (!user) return res.status(404).send("No user found.");


            var weight_array= [];
            weight_array.push({
                fecha: Date.now(),
                peso: user.peso
            })
            Weight.create({
                    weight_register:weight_array
                },
                function (err, weight) {
                    // if (err) return res.status(500).send("There was a problem registering the weight.");
                    console.log(weight._id+" desde controller - weight");

                    user.id_weight = weight._id;
                    user.save(function (err) {
                        if (err)
                            res.send(err +  "error weight")
                    });
                });

        });

    },


    show: function (req, res, next) {
        User.findById(req.userId, { password: 0 }, function (err, user) {
            if (err) return res.status(500).send("There was a problem finding the user.");
            if (!user) return res.status(404).send("No user found.");

            Weight.findById(user.id_weight, function (err, weight) {
                if (err) return res.status(500).send("There was a problem finding the nutrition.");
                if (!weight) return res.status(404).send("No nutrition found.");

                res.status(200).send(weight);
            });
        });
    },

    showLast: function (req, res, next) {
        User.findById(req.userId, { password: 0 }, function (err, user) {
            if (err) return res.status(500).send("There was a problem finding the user.");
            if (!user) return res.status(404).send("No user found.");

            Weight.find({'_id':user.id_weight}, function (err, weight) {
                if (err) return res.status(500).send("There was a problem finding the weight.");
                if (!weight) return res.status(404).send("No weight found.");

                res.status(200).send(weight);
            });
        });
    },


    add: function (req, res, next) {
        User.findById(req.userId, { password: 0 }, function (err, user) {
            if (err) return res.status(500).send("There was a problem finding the user.");
            if (!user) return res.status(404).send("No user found.");

            Weight.findById(user.id_weight, function (err, weight) {
                if (err) return res.status(500).send("There was a problem finding the nutrition.");
                if (!weight) return res.status(404).send("No nutrition found.");


                var weight_array= [weight.weight_register];
                console.log(weight_array + "anteeeeees");
                weight.weight_register.push({
                    fecha: Date.now(),
                    peso: req.body.peso
                })
                console.log(weight_array + "despuesssss");

                        //weight.weight_register = weight_array
                    weight.save(function (err, weight) {
                        // if (err) return res.status(500).send("There was a problem registering the weight.");
                        console.log(req.body.peso+" desde controller - weight");

                        user.id_weight = weight._id;
                        user.save(function (err) {
                            if (err)
                                res.send(err +  "error weight");
                            res.status(200).send(weight);
                        });
                    });
            });
        });
    }


};

