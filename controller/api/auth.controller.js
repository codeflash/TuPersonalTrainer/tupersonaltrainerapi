// AuthController.js
var express = require('express');
var router = express.Router();
var User = require('../../model/api/user.model');
var Nutrition = require('../../controller/api/nutritional.controller');
var Tips = require('../../controller/api/tips.controller');
var Training = require('../../controller/api/training.controller');
var Weight = require('../../controller/api/weight.controller');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../app/config');



module.exports = {

    register: function (req, res) {

        var hashedPassword = bcrypt.hashSync(req.body.password, 8);


        User.create({
                nombre: req.body.nombre,
                correo: req.body.correo,
                password: hashedPassword,
                telefono: req.body.telefono,
                objetivo: req.body.objetivo,
                edad:   req.body.edad,
                altura: req.body.altura,
                porcentaje_grasa: req.body.porcentaje_grasa,
                peso: req.body.peso,
                genero: req.body.genero
            },
            function (err, user) {
                if (err) return res.status(500).send("There was a problem registering the user."+ err.message)
                // create a token
                var token = jwt.sign({id: user._id}, config.secret, {
                    expiresIn: 2592000//86400 // expires in 24 hours
                });
                Nutrition.create(user._id);
                Tips.create(user._id);
                Training.create(user._id);
                //console.log(user.peso + "pesoooooooooo");
                Weight.create(user._id,user.peso);

                res.status(200).send({auth: true, token: token});
            });

    },


    me: function (req, res, next) {
        User.findById(req.userId, { password: 0, telefono: 0 }, function (err, user) {
            if (err) return res.status(500).send("There was a problem finding the user.");
            if (!user) return res.status(404).send("No user found.");

            res.status(200).send(user);
        });
    },


    update: function (req, res,next) {

        User.findById(req.userId, { password: 0, telefono: 0 }, function (err, user) {
            if (err) return res.status(500).send("There was a problem finding the user.");
            if (!user) return res.status(404).send("No user found.");

            console.log(req.body);
            User.findByIdAndUpdate(req.userId, req.body ,function (err, user) {
                if (err) return res.status(500).send("There was a problem finding the user.");
                if (!user) return res.status(404).send("No user found.");

                res.json({
                    success: true,
                    message: "Plato actualizado",
                    data: null
                })
            } )
        });
            
    },




    login: function (req, res) {
        User.findOne({correo: req.body.correo}, function (err, user) {
            if (err) return res.status(500).send('Error on the server.');
            if (!user) return res.status(404).send('No user found.');
            var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
            if (!passwordIsValid) return res.status(401).send({auth: false, token: null});
            var token = jwt.sign({id: user._id}, config.secret, {
                expiresIn: 86400 // expires in 24 hours
            });
            res.status(200).send({auth: true, token: token});
        });
    }
};

