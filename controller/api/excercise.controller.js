// ExcerciseController.js

var Excercise = require('../../model/admin/excercise.model');


module.exports = {

    show: function (req, res) {

        Excercise.find({},function (err, excercises) {
            if (err) return res.status(500).send("There was a problem finding the user.");
            if (!excercises) return res.status(404).send("No user found.");

            console.log("ejercicios",excercises);
            res.render('excercise_show', {excercises:excercises})
        });
    },
    showOne: function (req, res) {

        Excercise.findById(req.params.excercise_id,function (err, excercise) {
            if (err) return res.status(500).send("There was a problem finding the user.");
            if (!excercise) return res.status(404).send("No user found.");

            console.log("ejercicios",excercise);
            res.render('excercise_edit', {excercise:excercise})
        });
    },

};

